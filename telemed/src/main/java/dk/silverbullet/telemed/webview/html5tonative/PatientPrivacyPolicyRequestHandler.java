package dk.silverbullet.telemed.webview.html5tonative;

import org.json.JSONException;
import org.json.JSONObject;

import dk.silverbullet.telemed.MainActivity;
import dk.silverbullet.telemed.Util;
import dk.silverbullet.telemed.webview.nativetohtml5.WebViewMessageDispatcher;

public class PatientPrivacyPolicyRequestHandler implements MessageHandler {

    private final MainActivity mainActivity;
    private final WebViewMessageDispatcher messageDispatcher;

    public PatientPrivacyPolicyRequestHandler(MainActivity mainActivity, WebViewMessageDispatcher webViewMessageDispatcher) {

        this.mainActivity = mainActivity;
        messageDispatcher = webViewMessageDispatcher;
    }

    @Override
    public String getMessageTypeHandled() {
        return "patientPrivacyPolicyRequest";
    }

    @Override
    public void consume(JSONObject message) throws JSONException {

        String privacyText = Util.getPatientPrivacyPolicy(mainActivity);

        JSONObject response = new JSONObject();
        response.put("messageType", "patientPrivacyPolicyResponse");
        response.put("text", privacyText);

        messageDispatcher.send(response);
    }
}

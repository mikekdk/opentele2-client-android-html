package dk.silverbullet.telemed.webview.html5tonative;

import android.content.Intent;
import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;

import dk.silverbullet.telemed.MainActivity;

public class OpenUrlRequestHandler implements MessageHandler {

    private final MainActivity mainActivity;

    public OpenUrlRequestHandler(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public String getMessageTypeHandled() {
        return "openUrlRequest";
    }

    @Override
    public void consume(JSONObject message) throws JSONException {

        String url = message.getString("url");

        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        mainActivity.startActivity(i);
    }
}

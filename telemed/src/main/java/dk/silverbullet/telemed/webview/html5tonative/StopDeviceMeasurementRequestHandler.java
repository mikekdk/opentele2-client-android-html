package dk.silverbullet.telemed.webview.html5tonative;

import org.json.JSONException;
import org.json.JSONObject;

import dk.silverbullet.telemed.devices.DeviceHandler;

public class StopDeviceMeasurementRequestHandler implements MessageHandler {

    private final DeviceHandler deviceHandler;

    public StopDeviceMeasurementRequestHandler(DeviceHandler deviceHandler) {

        this.deviceHandler = deviceHandler;
    }

    @Override
    public String getMessageTypeHandled() {
        return "stopDeviceMeasurementRequest";
    }

    @Override
    public void consume(JSONObject message) throws JSONException {
        deviceHandler.removeDeviceListeners();
    }
}

package dk.silverbullet.telemed.webview.nativetohtml5;

import org.json.JSONObject;

public class LifeCycleEvents {

    private final NativeEventEmitter eventEmitter;

    public LifeCycleEvents(NativeEventEmitter eventEmitter) {

        this.eventEmitter = eventEmitter;
    }

    public void signalCreated() {
        eventEmitter.emitEvent(new JSONObject(), createLifeCycleEventHookFor("appCreated"));
    }

    public void signalResumed() {
        eventEmitter.emitEvent(new JSONObject(), createLifeCycleEventHookFor("appResumed"));
    }

    private Html5Hook createLifeCycleEventHookFor(String eventName) {
        return new Html5Hook("opentele-app", "lifeCycleEvents", eventName);
    }
}

package dk.silverbullet.telemed.webview.html5tonative;

import android.util.Log;
import android.webkit.JavascriptInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import dk.silverbullet.telemed.Util;

public class JsonEventHub implements JavaScriptInterface {

    private static final String TAG = Util.getTag(JsonEventHub.class);

    private Map<String, Set<MessageHandler>> messageHandlers = new HashMap<>();

    @JavascriptInterface
    public void notify(String message) throws JSONException {
        Log.d("JAVASCRIPT", "Message received from web view: " + message);
        JSONObject asJson = new JSONObject(message);
        publish(asJson);
    }

    public void subscribe(MessageHandler handler) {

        String messageType = handler.getMessageTypeHandled();
        if (!messageHandlers.containsKey(messageType)) {
            messageHandlers.put(messageType, new HashSet<MessageHandler>());
        }

        messageHandlers.get(messageType).add(handler);
    }

    public void publish(JSONObject message) throws JSONException {
        String messageType = message.getString("messageType");

        if (!messageHandlers.containsKey(messageType)) {
            Log.i(TAG, "No message handler found for message with type: " + messageType);
            return;
        }

        for (MessageHandler handler: messageHandlers.get(messageType)) {
            handler.consume(message);
        }
    }

    @Override
    public String getName() {
        return "external";
    }
}

package dk.silverbullet.telemed.webview.nativetohtml5;

import android.webkit.WebView;

import org.json.JSONException;
import org.json.JSONObject;

public class Html5WebViewMessageDispatcher implements WebViewMessageDispatcher {

    private final WebView webView;

    public Html5WebViewMessageDispatcher(WebView webView) {

        this.webView = webView;
    }

    @Override
    public void send(JSONObject message) throws JSONException {

        webView.loadUrl("javascript:var message = JSON.stringify(" + message.toString() +"); sendMessageToWebView(message);");
    }
}

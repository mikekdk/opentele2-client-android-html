package dk.silverbullet.telemed.webview.html5tonative;

import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import dk.silverbullet.telemed.MainActivity;
import dk.silverbullet.telemed.R;

public class ErrorPageInterface implements JavaScriptInterface {

    // --*-- Fields --*--

    private MainActivity mainActivity;
    private String openTeleUrl;
    private WebView webView;

    // --*-- Constructors --*--

    public ErrorPageInterface(MainActivity mainActivity, String openTeleUrl, WebView webView) {
        this.mainActivity = mainActivity;
        this.openTeleUrl = openTeleUrl;
        this.webView = webView;
    }

    // --*-- Methods --*--

    @Override
    public String getName() {
        return "ErrorPage";
    }

    @JavascriptInterface
    public String getTitleText() {
        return mainActivity.getString(R.string.app_name);
    }

    @JavascriptInterface
    public String getHeaderText() {
        return mainActivity.getString(R.string.error_header_text);
    }

    @JavascriptInterface
    public String getMessageText() {
        return mainActivity.getString(R.string.error_message_text);
    }

    @JavascriptInterface
    public String getButtonText() {
        return mainActivity.getString(R.string.reload_button_text);
    }

    @JavascriptInterface
    public String getServerUrl() {
        return openTeleUrl;
    }

    @JavascriptInterface
    public void reconnectButtonClicked() {
        mainActivity.runOnUiThread(new Runnable() {
            public void run() {
                webView.loadUrl(openTeleUrl);
            }
        });
    }

}

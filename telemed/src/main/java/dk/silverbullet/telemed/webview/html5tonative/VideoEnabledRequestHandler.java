package dk.silverbullet.telemed.webview.html5tonative;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import dk.silverbullet.telemed.Constants;
import dk.silverbullet.telemed.reflection.ReflectionHelper;
import dk.silverbullet.telemed.webview.nativetohtml5.WebViewMessageDispatcher;

public class VideoEnabledRequestHandler implements MessageHandler {

    private final Context context;
    private final WebViewMessageDispatcher webViewMessageDispatcher;

    public VideoEnabledRequestHandler(Context context, WebViewMessageDispatcher webViewMessageDispatcher) {

        this.context = context;
        this.webViewMessageDispatcher = webViewMessageDispatcher;
    }

    @Override
    public String getMessageTypeHandled() {
        return "videoEnabledRequest";
    }

    @Override
    public void consume(JSONObject message) throws JSONException {
        boolean isVideoEnabled = ReflectionHelper.classCanBeLoaded(context, Constants.VIDEO_PROVIDER_QUALIFIED_NAME);

        JSONObject response = new JSONObject();
        response.put("messageType", "videoEnabledResponse");
        response.put("enabled", isVideoEnabled);

        webViewMessageDispatcher.send(response);
    }
}

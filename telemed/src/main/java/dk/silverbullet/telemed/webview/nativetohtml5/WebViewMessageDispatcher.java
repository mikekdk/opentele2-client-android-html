package dk.silverbullet.telemed.webview.nativetohtml5;

import org.json.JSONException;
import org.json.JSONObject;

public interface WebViewMessageDispatcher {
    void send(JSONObject message) throws JSONException;
}

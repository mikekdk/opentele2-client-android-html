package dk.silverbullet.telemed.webview.nativetohtml5;

import org.json.JSONObject;

import dk.silverbullet.telemed.webview.nativetohtml5.Html5Hook;

public interface NativeEventEmitter {

    void emitEvent(JSONObject event, Html5Hook html5Hook);

}

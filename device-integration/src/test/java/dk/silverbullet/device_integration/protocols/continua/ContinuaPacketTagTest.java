package dk.silverbullet.device_integration.protocols.continua;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ContinuaPacketTagTest {

	// --*-- Tests --*--

	@Test
	public void canFindTagForValue() {
		assertEquals(ContinuaPacketTag.AARQ_APDU,
                ContinuaPacketTag.packetTagForValue(0xE200));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void failsWhenFindingTagForUnknownValue() {
		ContinuaPacketTag.packetTagForValue(123);
	}
	
	@Test
	public void knowsWhichTagValuesAreDefined() {
		assertTrue(ContinuaPacketTag.isKnownTagValue(0xE400));
	}
	
	@Test
	public void knowsWhichTagValuesAreUnknown() {
		assertFalse(ContinuaPacketTag.isKnownTagValue(123));
	}
}

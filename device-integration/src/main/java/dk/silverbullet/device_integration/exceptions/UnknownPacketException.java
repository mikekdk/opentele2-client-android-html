package dk.silverbullet.device_integration.exceptions;

public class UnknownPacketException extends InvalidPacketException {

    private static final long serialVersionUID = 5099733625887801546L;

    public UnknownPacketException(String message) {
        super(message);
    }

    public UnknownPacketException() {
        super();
    }
}


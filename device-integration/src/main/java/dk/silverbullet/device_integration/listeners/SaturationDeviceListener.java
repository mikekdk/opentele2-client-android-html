package dk.silverbullet.device_integration.listeners;

import android.app.Activity;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import dk.silverbullet.device_integration.Util;
import dk.silverbullet.device_integration.devices.noninsaturation.NoninController;
import dk.silverbullet.device_integration.devices.noninsaturation.SaturationAndPulse;
import dk.silverbullet.device_integration.devices.noninsaturation.SaturationPulseListener;
import dk.silverbullet.device_integration.exceptions.DeviceInitialisationException;
import dk.silverbullet.device_integration.protocols.continua.DeviceInformation;

public class SaturationDeviceListener implements SaturationPulseListener {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(SaturationDeviceListener.class);

    private final Activity activity;
    private final OnEventListener onEventListener;
    private final DefaultDeviceListener defaultDeviceListener;
    private NoninController noninController;

    // --*-- Constructors --*--

    public SaturationDeviceListener(Activity activity,
                                    OnEventListener onEventListener) {
        this.activity = activity;
        this.onEventListener = onEventListener;
        this.defaultDeviceListener =
                new DefaultDeviceListener(activity, onEventListener);
    }

    // --*-- Methods --*--

    @Override
    public void start() {
        Log.d(TAG, "Start");
        try {
            defaultDeviceListener.sendStatusEvent(JSONConstants.INFO, JSONConstants.SATURATION_CONNECTING);
            noninController = new NoninController(this);
            noninController.start();
        } catch (DeviceInitialisationException e) {
            e.printStackTrace();
            defaultDeviceListener.sendStatusEvent(JSONConstants.ERROR, JSONConstants.SATURATION_COULD_NOT_CONNECT);
        }
    }

    @Override
    public void stop() {
        Log.d(TAG, "Stop");
        if (noninController != null) {
            noninController.close();
        }
    }

    @Override
    public void connecting() {
        Log.d(TAG, "Connecting");
        defaultDeviceListener.sendStatusEvent(JSONConstants.INFO, JSONConstants.SATURATION_CONNECTING);
    }

    @Override
    public void connected() {
        Log.d(TAG, "Connected");
        defaultDeviceListener.sendStatusEvent(JSONConstants.INFO, JSONConstants.SATURATION_CONNECTED);
    }

    @Override
    public void disconnected() {
        Log.d(TAG, "Disconnected");
        defaultDeviceListener.sendStatusEvent(JSONConstants.INFO, JSONConstants.SATURATION_DISCONNECTED);
    }

    @Override
    public void permanentProblem() {
        Log.d(TAG, "Permanent problem");
        defaultDeviceListener.sendStatusEvent(JSONConstants.ERROR, JSONConstants.SATURATION_PERMANENT_PROBLEM);
    }

    @Override
    public void temporaryProblem() {
        Log.d(TAG, "Temporary problem");
        defaultDeviceListener.sendStatusEvent(JSONConstants.ERROR, JSONConstants.SATURATION_TEMPORARY_PROBLEM);
    }

    @Override
    public void firstTimeOut() {
        Log.d(TAG, "First time out");
        defaultDeviceListener.sendStatusEvent(JSONConstants.INFO, JSONConstants.SATURATION_FIRST_TIMEOUT);
    }

    @Override
    public void finalTimeOut(DeviceInformation deviceInformation, SaturationAndPulse saturationAndPulse) {
        Log.d(TAG, "Final time out");
        if (saturationAndPulse == null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    noninController.close();
                    try {
                        JSONObject timeoutEvent = defaultDeviceListener.createStatusEvent(
                                JSONConstants.INFO, JSONConstants.SATURATION_FINAL_TIMEOUT);
                        onEventListener.onEvent(timeoutEvent);
                    } catch (JSONException e) {
                        Log.e(TAG, "Could not create timeout event");
                        Log.e(TAG, e.getMessage());
                        e.printStackTrace();
                    }
                }
            });
        } else {
            measurementReceived(deviceInformation, saturationAndPulse);
        }
    }

    @Override
    public void measurementReceived(final DeviceInformation deviceInformation,
                                    final SaturationAndPulse saturationAndPulse) {
        Log.d(TAG, "Measurement received");
        Log.d(TAG, saturationAndPulse.toString());
        Log.d(TAG, deviceInformation.toString());

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                noninController.close();
                try {
                    Date timestamp = new Date();
                    JSONObject saturationEvent = createSaturationEvent(deviceInformation, timestamp, saturationAndPulse);
                    JSONObject pulseEvent = createPulseEvent(deviceInformation, timestamp, saturationAndPulse);
                    Log.d(TAG, saturationEvent.toString());
                    Log.d(TAG, pulseEvent.toString());
                    onEventListener.onEvent(saturationEvent);
                    onEventListener.onEvent(pulseEvent);
                    disconnected();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private JSONObject createSaturationEvent(DeviceInformation deviceInformation,
                                             Date timestamp,
                                             SaturationAndPulse saturation) throws JSONException {

        JSONObject event = new JSONObject();
        event.put(JSONConstants.TIMESTAMP, timestamp.toString());
        event.put(JSONConstants.DEVICE, defaultDeviceListener.createDeviceObject(deviceInformation));
        event.put(JSONConstants.TYPE, JSONConstants.MEASUREMENT);
        JSONObject measurement = new JSONObject();
        measurement.put(JSONConstants.TYPE, JSONConstants.SATURATION);
        measurement.put(JSONConstants.UNIT, JSONConstants.PERCENTAGE);
        measurement.put(JSONConstants.VALUE, saturation.getSaturation());
        event.put(JSONConstants.MEASUREMENT, measurement);
        return event;
    }

    private JSONObject createPulseEvent(DeviceInformation deviceInformation,
                                        Date timestamp,
                                        SaturationAndPulse pulse) throws JSONException {

        JSONObject event = new JSONObject();
        event.put(JSONConstants.TIMESTAMP, timestamp.toString());
        event.put(JSONConstants.DEVICE, defaultDeviceListener.createDeviceObject(deviceInformation));
        event.put(JSONConstants.TYPE, JSONConstants.MEASUREMENT);
        JSONObject measurement = new JSONObject();
        measurement.put(JSONConstants.TYPE, JSONConstants.PULSE);
        measurement.put(JSONConstants.UNIT, JSONConstants.BPM);
        measurement.put(JSONConstants.VALUE, pulse.getPulse());
        event.put(JSONConstants.MEASUREMENT, measurement);
        return event;
    }

}

package dk.silverbullet.device_integration.protocols.continua.packet.input;

public enum AssociateResult {

    ACCEPTED,
    REJECTED_PERMANENT,
    REJECTED_TRANSIENT,
    ACCEPTED_UNKNOWN_CONFIG,
    REJECTED_NO_COMMON_PROTOCOL,
    REJECTED_NO_COMMON_PARAMETER,
    REJECTED_UNKNOWN,
    REJECTED_UNAUTHORIZED,
    REJECTED_UNSUPPORTED_ASSOC_VERSION;

    public static int getValue(AssociateResult result) {
        switch (result) {
            case ACCEPTED:
                return 0x0000;
            case REJECTED_PERMANENT:
                return 0x0001;
            case REJECTED_TRANSIENT:
                return 0x0002;
            case ACCEPTED_UNKNOWN_CONFIG:
                return 0x0003;
            case REJECTED_NO_COMMON_PROTOCOL:
                return 0x0004;
            case REJECTED_NO_COMMON_PARAMETER:
                return 0x0005;
            case REJECTED_UNKNOWN:
                return 0x0006;
            case REJECTED_UNAUTHORIZED:
                return 0x0007;
            case REJECTED_UNSUPPORTED_ASSOC_VERSION:
                return 0x0008;
            default:
                return -1;
        }
    }

}

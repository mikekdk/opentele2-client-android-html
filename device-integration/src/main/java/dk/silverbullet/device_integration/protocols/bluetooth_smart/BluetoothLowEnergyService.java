package dk.silverbullet.device_integration.protocols.bluetooth_smart;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import java.util.List;
import java.util.UUID;

import dk.silverbullet.device_integration.Util;

public class BluetoothLowEnergyService extends Service {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(BluetoothLowEnergyService.class);

    private BluetoothManager bluetoothManager;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothGatt bluetoothGatt;
    private String bluetoothDeviceAddress;

    private int connectionState = GattConstants.STATE_DISCONNECTED;

    // --*-- Methods --*--

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handleCommand(intent);
        return START_STICKY;
    }

    private void handleCommand(Intent intent) {

        if (intent == null || !intent.hasExtra(GattConstants.EXTRA_DATA_DEVICE_ADDRESS)) {
            Log.e(TAG, "Intent did not have EXTRA_DATA_DEVICE_ADDRESS");
            return;
        }
        String deviceAddress = intent.getStringExtra(GattConstants.EXTRA_DATA_DEVICE_ADDRESS);
        Log.d(TAG, "deviceAddress: " + deviceAddress);

        if (!intent.hasExtra(GattConstants.EXTRA_DATA_SERVICE_ID)) {
            Log.e(TAG, "Intent did not have EXTRA_DATA_SERVICE_ID");
            return;
        }
        UUID serviceId = (UUID) intent.getSerializableExtra(GattConstants.EXTRA_DATA_SERVICE_ID);

        if (!intent.hasExtra(GattConstants.EXTRA_DATA_CHARACTERISTIC_ID)) {
            Log.e(TAG, "Intent did not have EXTRA_DATA_CHARACTERISTIC_ID");
            return;
        }
        UUID characteristicId = (UUID) intent.getSerializableExtra(GattConstants.EXTRA_DATA_CHARACTERISTIC_ID);

        boolean initialized = initialize();
        if (!initialized) {
            Log.e(TAG, "Could not initialize Bluetooth low energy Service");
            return;
        }

        boolean connected = connect(deviceAddress, serviceId, characteristicId);
        Log.d(TAG, "Connected: " + connected);
    }

    public boolean initialize() {

        if (bluetoothManager == null) {
            bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (bluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        bluetoothAdapter = bluetoothManager.getAdapter();
        if (bluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     * @param serviceId -
     * @param characteristicId -
     * @return Return true if the connection is initiated successfully.
     */
    public boolean connect(final String address, final UUID serviceId,
                           final UUID characteristicId) {

        if (bluetoothAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        // Previously connected device. Try to reconnect.
        if (bluetoothDeviceAddress != null &&
                address.equals(bluetoothDeviceAddress) &&
                bluetoothGatt != null) {
            Log.d(TAG, "Trying to use an existing bluetoothGatt for connection.");

            if (bluetoothGatt.connect()) {
                connectionState = GattConstants.STATE_CONNECTING;
                return true;
            } else {
                return false;
            }
        }

        if (!BluetoothAdapter.checkBluetoothAddress(address)) {
            Log.d(TAG, "Address: " + address + " is not a valid bluetooth address");
            return false;
        }
        final BluetoothDevice device = bluetoothAdapter.getRemoteDevice(address);
        Log.d(TAG, "Device: " + device.toString());

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                GattCallback gattCallback = new GattCallback(BluetoothLowEnergyService.this,
                        serviceId, characteristicId);
                boolean autoConnect = false; // TODO: Check if false or true works best
                bluetoothGatt = device.connectGatt(getApplicationContext(), autoConnect, gattCallback);
                Log.d(TAG, "Trying to create a new connection.");
            }
        });

        bluetoothDeviceAddress = address;
        connectionState = GattConstants.STATE_CONNECTING;
        return true;
    }

    /**
     * Disconnects an existing connection or cancel a pending connection.
     */
    public void disconnect() {
        if (bluetoothAdapter == null || bluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        bluetoothGatt.disconnect();
    }

    /**
     * Called when we are done with a device and want to release resources.
     */
    public void close() {
        if (bluetoothGatt == null) {
            return;
        }
        bluetoothGatt.close();
        bluetoothGatt = null;
    }

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}.
     *
     * @param characteristic The characteristic to read from.
     */
    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (bluetoothAdapter == null || bluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        bluetoothGatt.readCharacteristic(characteristic);
    }

    /**
     * Enables or disables notification on a give characteristic.
     *
     * @param characteristic Characteristic to act on.
     * @param enabled If true, enable notification. False otherwise.
     */
    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic,
                                              boolean enabled) {

        if (bluetoothAdapter == null || bluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        bluetoothGatt.setCharacteristicNotification(characteristic, enabled);

        Log.d(TAG, "Setting descriptor");
        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(
                GattConstants.CLIENT_CHARACTERISTIC_CONFIG_CHARACTERISTIC);
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        boolean descriptorStarted = bluetoothGatt.writeDescriptor(descriptor);
        Log.d(TAG, "descriptor started: " + descriptorStarted);

    }

    public List<BluetoothGattService> getSupportedGattServices() {
        if (bluetoothGatt == null) {
            return null;
        }
        return bluetoothGatt.getServices();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    // -*- Getters/Setters -*-

    public BluetoothGatt getBluetoothGatt() {
        return bluetoothGatt;
    }

    public int getConnectionState() {
        return connectionState;
    }

    public void setConnectionState(int connectionState) {
        this.connectionState = connectionState;
    }

}

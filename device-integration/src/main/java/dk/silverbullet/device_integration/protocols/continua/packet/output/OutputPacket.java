package dk.silverbullet.device_integration.protocols.continua.packet.output;

public interface OutputPacket {
    byte[] getContents();
}

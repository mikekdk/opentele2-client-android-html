package dk.silverbullet.device_integration.protocols.continua;

public interface HdpListener {
    void applicationConfigurationRegistered();

    void applicationConfigurationRegistrationFailed();

    void applicationConfigurationUnregistered();

    void applicationConfigurationUnregistrationFailed();

    void serviceConnectionFailed();

    void connectionEstablished();

    void disconnected();
}

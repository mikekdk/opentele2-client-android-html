package dk.silverbullet.device_integration;

public final class Util {

    // --*-- Constructors --*--

    private Util() {
        throw new RuntimeException("Util cannot be instantiated.");
    }

    // --*-- Methods --*--

    public static String getTag(Class<?> cls) {
        return cls.getName().substring(cls.getName().lastIndexOf(".") + 1);
    }

}

package dk.silverbullet.device_integration.devices.noninsaturation.packet.states;

import android.util.Log;

import dk.silverbullet.device_integration.Util;
import dk.silverbullet.device_integration.devices.noninsaturation.packet.NoninPacketCollector;

public class WaitForDataFormatAckState extends ReceiverState {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(WaitForDataFormatAckState.class);

    private NoninPacketCollector noninPacketCollector;

    // --*-- Constructors --*--

    public WaitForDataFormatAckState(NoninPacketCollector noninPacketCollector) {
        super(noninPacketCollector);
        this.noninPacketCollector = noninPacketCollector;
    }

    // --*-- Methods --*--

    @Override
    public boolean receive(int in) {
        if (in == ACK || 0x15 == in) {
            if (0x15 == in) {
                Log.d(TAG, "!!! We got an 'unkown data format' response, but are continuing");
            }
            noninPacketCollector.clearBuffer();
            noninPacketCollector.receivedDataFormatChanged();
            return true;
        } else {
            Log.d(TAG, "Expected ACK but got:" + Integer.toHexString(in));
        }

        return false;
    }

    @Override
    public void entering() {

    }

}

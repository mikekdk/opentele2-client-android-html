package dk.silverbullet.device_integration.devices.andbloodpressure.continua.packet.input;

import android.util.Log;

import java.io.IOException;

import dk.silverbullet.device_integration.Util;
import dk.silverbullet.device_integration.protocols.continua.DeviceInformation;
import dk.silverbullet.device_integration.protocols.continua.packet.input.ConfirmedMeasurementDataPacket;
import dk.silverbullet.device_integration.protocols.continua.packet.input.OrderedByteReader;
import dk.silverbullet.device_integration.exceptions.UnexpectedPacketFormatException;

public class AndBloodPressureConfirmedMeasurementDataPacket extends ConfirmedMeasurementDataPacket {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(AndBloodPressureConfirmedMeasurementDataPacket.class);

    private static final long NO_TIME = -1;

    private long newestTimestampForBloodPressure;
    private long newestTimestampForPulse;

    private int systolicBloodPressure;
    private int diastolicBloodPressure;
    private int meanArterialPressure;
    private int pulse;
    private DeviceInformation deviceInformation;

    // --*-- Constructors --*--

    public AndBloodPressureConfirmedMeasurementDataPacket(byte[] contents) throws IOException {
        super(contents);
    }

    // --*-- Methods --*--

    @Override
    protected void readObjects(int eventType, OrderedByteReader in) throws IOException {

        switch (eventType) {
            case MDC_ATTRIBUTES:
                readMedicalDeviceSystemAttributes(in);
                break;
            case MDC_NOTI_SCAN_REPORT_FIXED:
                readMeasurement(eventType, in);
                break;
            case MDC_NOTI_CONFIG:
                readNewProtocolConfigurationMeasurement(in);
                break;
            default:
                throw new UnexpectedPacketFormatException("Unexpected event-type 0x" +
                        Integer.toString(eventType, 16) +
                        " (expected 0x0D1E or 0x0D1F)");
        }

    }

    private void readMedicalDeviceSystemAttributes(OrderedByteReader in) throws IOException {
        Log.d(TAG, "readMedicalDeviceSystemAttributes");

        int deviceAttributeListCount = in.readShort(); //attribute-list.count = 10
        if (deviceAttributeListCount != 10) {
            // TODO: More dynamic parsing of MDS Attributes response
            Log.d(TAG, "Unknown deviceInformation, returning");
            return;
        }
        checkShort(in, "attribute-list.length = 178", 0x00B2);
        checkShort(in, "attribute-id = MDC_ATTR_SYS_TYPE_SPEC_LIST", 0x0A5A);
        checkShort(in, "attribute-value.length = 8", 0x0008);
        checkShort(in, "TypeVerList.count = 1", 0x0001);
        checkShort(in, "TypeVerList.length = 4", 0x0004);
        checkShort(in, "type = MDC_DEV_SPEC_PROFILE_BP", 0x1007);
        checkShort(in, "version = version 1 of the specialization", 0x0001);
        checkShort(in, "attribute-id = MDC_ATTR_ID_MODEL", 0x0928);
        checkShort(in, "attribute-value.length = 28", 0x001C);
        int manufacturerLength = in.readShort(); // string length = 12
        String manufacturer = readString(in, manufacturerLength-1);
        in.readByte();
        int modelLength = in.readShort(); // string length = 12
        String model = readString(in, modelLength);
        checkShort(in, "attribute-id = MDC_ATTR_SYS_ID", 0x0984);
        checkShort(in, "attribute-value.length = 10", 0x000A);
        checkShort(in, "octet string length = 8", 0x0008);
        String eui64 = String.valueOf(in.readLong());
        checkShort(in, "attribute-id = MDC_ATTR_DEV_CONFIG_ID", 0x0A44);
        checkShort(in, "attribute-value.length = 2", 0x0002);
        checkShort(in, "dev-config-id = 16384 (extended-config-start)", 0x4000);
        checkShort(in, "attribute-id = MDC_ATTR_ID_PROD_SPECN", 0x092D);
        checkShort(in, "attribute-value.length = 40", 0x0028);
        checkShort(in, "ProductionSpec.count = 2", 0x0002);
        checkShort(in, "ProductionSpec.length = 36", 0x0024);
        checkShort(in, "ProdSpecEntry.spec-type = 1 (serial-number)", 0x0001);
        checkShort(in, "ProdSecpEntry.component-id = 0", 0x0000);
        int serialNumberLength = in.readShort(); // string length = 10
        String serialNumber = readString(in, serialNumberLength);
        checkShort(in, "ProdSpecEntry.spec-type = 5 (fw-revision)", 0x0005);
        checkShort(in, "ProdSpecEntry.component-id = 0", 0x0000);
        int firmwareRevisionLength = in.readShort(); // string length = 14
        String firmwareRevision = readString(in, firmwareRevisionLength);
        checkShort(in, "attribute-id = MDC_ATTR_TIME_ABS", 0x0987);
        checkShort(in, "attribute-value.length = 8", 0x0008);
        readTimestamp(in);
        checkShort(in, "attribute-id = MDC_ATTR_REG_CERT_DATA_LIST", 0x0A4B);
        checkShort(in, "attribute-value.length = 22", 0x0016);
        checkShort(in, "auth-body-continua", 0x0002);
        checkShort(in, "??", 0x0012);
        checkShort(in, "??", 0x0201);
        checkShort(in, "??", 0x0008);
        checkShort(in, "??", 0x0105);
        checkShort(in, "??", 0x0001);
        checkShort(in, "??", 0x0002);
        checkShort(in, "??", 0x4007);
        checkShort(in, "??", 0x0202);
        checkShort(in, "??", 0x0002);
        checkShort(in, "??", 0x0000);
        checkShort(in, "attribute-id = MDC_ATTR_MDS_TIME_INFO", 0x0A45);
        checkShort(in, "attribute-value.length = 16", 0x0010);
        checkShort(in, "??", 0xC010);
        checkShort(in, "??", 0x1F00);
        checkShort(in, "??", 0xFFFF);
        checkShort(in, "??", 0xFFFF);
        checkShort(in, "??", 0x0064);
        checkShort(in, "??", 0x0000);
        checkShort(in, "??", 0x0000);
        checkShort(in, "??", 0x0000);
        checkShort(in, "attribute-id = MDC_ATTR_POWER_STAT", 0x0955);
        checkShort(in, "attribute-value.length = 2", 0x0002);
        checkShort(in, "dev-config-id = 16384 (extended-config-start)", 0x4000);
        checkShort(in, "attribute-id = MDC_ATTR_UNIT_CODE", 0x099C);
        checkShort(in, "attribute-value.length = 2", 0x0002);
        checkShort(in, "??", 0x0064);

        deviceInformation = new DeviceInformation(model, eui64,
                firmwareRevision, manufacturer, serialNumber, null);
    }

    private void readMeasurement(int eventType, OrderedByteReader in) throws IOException {
        Log.d(TAG, "readMeasurement");

        if (eventType != MDC_NOTI_SCAN_REPORT_FIXED) {
            throw new UnexpectedPacketFormatException("Unexpected event-type 0x" +
                    Integer.toString(eventType, 16) + "(expected 0x0D1D)");
        }

        newestTimestampForBloodPressure = NO_TIME;
        newestTimestampForPulse = NO_TIME;

        int numberOfMeasurements = in.readShort(); // ScanReportInfoFixed.obs-scan-fixed.count
        in.readShort(); // ScanReportInfoFixed.obs-scan-fixed.length

        for (int i = 0; i < numberOfMeasurements; i++) {
            int objectHandle = in.readShort(); // ScanReportInfoFixed.obs-scan-fixed.value[0].obj-handle
            if (objectHandle == 1) {
                readBloodPressureMeasurement(in);
            } else if (objectHandle == 2) {
                readPulseMeasurement(in);
            } else if (objectHandle == 3) {
                Log.d(TAG, "Found A&D-specific measurement, ignoring it.");
            } else {
                throw new UnexpectedPacketFormatException("Unexpected object handle: '" + objectHandle + "'");
            }
        }
    }

    private void readNewProtocolConfigurationMeasurement(OrderedByteReader in) throws IOException {
        Log.i(TAG, "readNewProtocolConfigurationMeasurement");

        // Below we check that we receive the expected configuration object,
        // ideally we should a new parser from the configuration object to parse
        // the subsequent blood pressure and pulse measurements.

        checkShort(in, "Number of measurements configuration", 0x0003); // config-obj-list.count = 3
        checkShort(in, "Length of config message", 0x0098); // config-obj-list.length = 152

        // First measurement configuration
        Log.d(TAG, "reading first measurement configuration");
        checkShort(in, "object class", 0x0006); // obj-class = MDC_MOC_VMO_METRIC_NU
        checkShort(in, "obj handle", 0x0002); // obj-handle = 2 (-> 1st measurement is pulse)
        checkShort(in, "attributes count", 0x0004); // attributes.count = 4
        checkShort(in, "attributes length", 0x0024); // attributes.length = 36
        checkShort(in, "attribute id", 0x092F); // attribute-id = MDC_ATTR_ID_TYPE
        checkShort(in, "attribute value length", 0x0004); // attribute-value.length = 4
        checkShort(in, "mdc part scada", 0x0002); // MDC_PART_SCADA
        checkShort(in, "mdc puls rate non inv", 0x482A); // MDC_PULS_RATE_NON_INV
        checkShort(in, "attribute id", 0x0A46); // attribute-id = MDC_ATTR_METRIC_SPEC_SMALL
        checkShort(in, "attribute value length", 0x0002); // attribute-value.length = 2
        checkShort(in, "fluff", 0xF040); // intermittent, Stored data, upd & msmst aperiodic, agent init, measured
        checkShort(in, "attribute id", 0x0996); // attribute-id = MDC_ATTR_UNIT_CODE
        checkShort(in, "attribute value length", 0x0002); // 0x00 0x02 attribute-value.length = 2
        checkShort(in, "beats per minute", 0x0AA0); // MDC_DIM_BEAT_PER_MIN
        checkShort(in, "attribute id", 0x0A55); // attribute-id = MDC_ATTR_ATTRIBUTE_VALUE_MAP
        checkShort(in, "attribute value length", 0x000C); // attribute-value.length = 12
        checkShort(in, "attribute value map count", 0x0002); // AttrValMap.count = 2
        checkShort(in, "attribute value map length", 0x0008); // AttrValMap.length = 8
        checkShort(in, "mdc attr nu val obs basic", 0x0A4C); // MDC_ATTR_NU_VAL_OBS_BASIC
        checkShort(in, "value length", 0x0002); // value length = 2
        checkShort(in, "mdc attr time stamp abs", 0x0990); // MDC_ATTR_TIME_STAMP_ABS
        checkShort(in, "value length", 0x0008); // value length = 8

        // Second measurement configuration
        Log.d(TAG, "reading second measurement configuration");
        checkShort(in, "object class", 0x0006); // obj-class = MDC_MOC_VMO_METRIC_NU
        checkShort(in, "object handle", 0x0001); // obj-handle = 1 (-> 2nd measurement is systolic, diastolic and mean arterial pressure)
        checkShort(in, "attributes count", 0x0006); // attributes.count = 6
        checkShort(in, "attributes length", 0x0038); // attributes.length = 56
        checkShort(in, "attribute id", 0x092F); // attribute-id = MDC_ATTR_ID_TYPE
        checkShort(in, "attribute value length", 0x0004); // attribute-value.length = 4
        checkShort(in, "mdc part scada", 0x0002); // MDC_PART_SCADA
        checkShort(in, "mdc press bld noninv", 0x4A04); // MDC_PULS_RATE_NON_INV
        checkShort(in, "attribute id", 0x0A46); // attribute-id = MDC_ATTR_METRIC_SPEC_SMALL
        checkShort(in, "attribute value length", 0x0002); // attribute-value.length = 2
        checkShort(in, "fluff", 0xF040); // intermittent, Stored data, upd & msmst aperiodic, agent init, measured
        checkShort(in, "mdc attr metric struct small", 0x0A73); // attribute-id = MDC_ATTR_METRIC_STRUCT_SMALL
        checkShort(in, "attribute value length", 0x0002); // attribute-value.length = 2
        checkShort(in, "ms struct compound fix", 0x0303); // {ms-struct-compound-fix, 3}
        checkShort(in, "mdc attr id physio list", 0x0A76); // attribute-id = MDC_ATTR_ID_PHYSIO_LIST
        checkShort(in, "attribute-value.length", 0x000A); // attribute-value.length = 10
        checkShort(in, "MetricIdList.count", 0x0003); // MetricIdList.count = 3
        checkShort(in, "MetricIdList.length", 0x0006); // MetricIdList.length = 6
        checkShort(in, "mdc press bld noninv sys", 0x4A05); // {MDC_PRESS_BLD_NONINV_SYS,
        checkShort(in, "mdc press bld noninv dia", 0x4A06); // MDC_PRESS_BLD_NONINV_DIA,
        checkShort(in, "mdc press bld noninv mean", 0x4A07); // MDC_PRESS_BLD_NONINV_MEAN}
        checkShort(in, "mdc attr unit code", 0x0996); // attribute-id = MDC_ATTR_UNIT_CODE
        checkShort(in, "attribute-value.length", 0x0002); // attribute-value.length = 2
        checkShort(in, "mmHg", 0x0F20); // MDC_DIM_MMHG
        checkShort(in, "mdc attr attribute val map", 0x0A55); // attribute-id = MDC_ATTR_ATTRIBUTE_VAL_MAP
        checkShort(in, "attribute-value.length", 0x000C); // attribute-value.length = 12
        checkShort(in, "AttrValMap.count", 0x0002); // AttrValMap.count = 2
        checkShort(in, "AttrValMap.length", 0x0008); // AttrValMap.length = 8
        checkShort(in, "mdc attr nu cmpd val obs basic", 0x0A75); // MDC_ATTR_NU_CMPD_VAL_OBS_BASIC
        checkShort(in, "value length", 0x000A); // value length = 10
        checkShort(in, "mdc attr time stamp abs", 0x0990); // MDC_ATTR_TIME_STAMP_ABS
        checkShort(in, "value length", 0x0008); // value length = 8

        // Third measurement configuration
        checkShort(in, "object class", 0x0005); // obj-class = ??
        checkShort(in, "object handle", 0x0003); // obj-handle = 3 (-> 3rd measurement is A&D specific)
        checkShort(in, "attributes.count", 0x0004); // attributes.count = 4
        checkShort(in, "attributes.length", 0x0024); // attribute.length = 36
        // We stop reading the object here, since we do not really care about its content.

        Log.d(TAG, "Successfully read configuration measurement");
    }

    private void readBloodPressureMeasurement(OrderedByteReader in) throws IOException {
        Log.i(TAG, "readBloodPressureMeasurement");

        checkShort(in, "ScanReportInfoFixed.obs-scan-fixed.value[0].obs-val-data.length", 18);
        checkShort(in, "Compound Object count", 3);
        checkShort(in, "Compound Object length", 6);
        int currentSystolicBloodPressure = in.readShort();
        int currentDiastolicBloodPressure = in.readShort();
        int currentMeanArterialPressure = in.readShort();
        long timestamp = in.readLong();
        if (timestamp > newestTimestampForBloodPressure) {
            systolicBloodPressure = currentSystolicBloodPressure;
            diastolicBloodPressure = currentDiastolicBloodPressure;
            meanArterialPressure = currentMeanArterialPressure;
            newestTimestampForBloodPressure = timestamp;
        }
        Log.d(TAG, "systolic: " + systolicBloodPressure);
        Log.d(TAG, "diastolic: " + diastolicBloodPressure);
        Log.d(TAG, "mean arterial pressure: " + meanArterialPressure);
        Log.d(TAG, "newestTimestamp (blood pressure): " + newestTimestampForBloodPressure);
    }

    private void readPulseMeasurement(OrderedByteReader in) throws IOException {
        Log.i(TAG, "readPulseMeasurement");

        checkShort(in, "ScanReportInfoFixed.obs-scan-fixed.value[0].obs-val-data.length", 10);
        int currentPulse = in.readShort();
        long timestamp = in.readLong();
        if (timestamp > newestTimestampForPulse) {
            pulse = currentPulse;
            newestTimestampForPulse = timestamp;
        }
        Log.d(TAG, "pulse: " + pulse);
        Log.d(TAG, "newestTimestamp (pulse): " + newestTimestampForPulse);
    }

    private long readTimestamp(OrderedByteReader in) throws IOException {
        return in.readLong();
    }

    private String readString(OrderedByteReader in, int length) throws IOException {
        StringBuilder stringBuilder = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            stringBuilder.append((char) in.readByte());
        }
        return stringBuilder.toString();
    }

    @Override
    public String toString() {
        return "AndBloodPressureConfirmedMeasurementDataPacket" +
                " [newestTimestampForBloodPressure=" + newestTimestampForBloodPressure +
                ", newestTimestampForPulse=" + newestTimestampForPulse +
                ", systolicBloodPressure=" + systolicBloodPressure +
                ", diastolicBloodPressure=" + diastolicBloodPressure +
                ", meanArterialPressure=" + meanArterialPressure +
                ", pulse=" + pulse +
                ", deviceInformation=" + deviceInformation +
                "]";
    }

    // -*- Getters -*-

    public int getSystolicBloodPressure() {
        return systolicBloodPressure;
    }

    public int getDiastolicBloodPressure() {
        return diastolicBloodPressure;
    }

    public int getMeanArterialPressure() {
        return meanArterialPressure;
    }

    public int getPulse() {
        return pulse;
    }

    public boolean hasBloodPressure() {
        return newestTimestampForBloodPressure != NO_TIME;
    }

    public boolean hasPulse() {
        return newestTimestampForPulse != NO_TIME;
    }

    public long getBloodPressureTimestamp() {
        return newestTimestampForBloodPressure;
    }

    public long getPulseTimestamp() {
        return newestTimestampForPulse;
    }

    public boolean hasDeviceInformation() {
        return  deviceInformation != null;
    }

    public DeviceInformation getDeviceInformation() {
        return deviceInformation;
    }

}

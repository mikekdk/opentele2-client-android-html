package dk.silverbullet.device_integration.devices.noninsaturation.packet.states;

import dk.silverbullet.device_integration.Util;
import dk.silverbullet.device_integration.devices.noninsaturation.packet.NoninPacketCollector;

public class WaitForSerialNumberState extends ReceiverState {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(WaitForSerialNumberState.class);

    // --*-- Constructors --*--

    public WaitForSerialNumberState(NoninPacketCollector stateController) {
        super(stateController);
    }

    // --*-- Methods --*--

    @Override
    public boolean receive(int in) {
        if (in == STX) {
            stateController.clearBuffer();
            stateController.addInt(in);
            stateController.setState(stateController.SERIAL_NUMBER_DATA_STATE);
            // Forward byte to new state (as it is actually the first byte of
            // the serial packet
            return stateController.receive(in);
        }
        return false;
    }

    @Override
    public void entering() {

    }
}
